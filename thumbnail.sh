#!/usr/bin/env bash

: "${BLENDER_PATH:=/usr/local/blender/blender}"
: "${BUILD_PATH:=build}"

blender() {
  "${BLENDER_PATH}" "$@"
}

source="${1}"
name="${source%.blend}"
frame="${2}"
output="$( printf '%06d' "${frame}" )"
blender -b "${source}" -S '3D' -o "//${BUILD_PATH}/######" -x 1 -F PNG -f "${frame}"
mv "${BUILD_PATH}/${output}.png" "${BUILD_PATH}/${name}_tn.png"
set -x
# Audio: mp3 (mp4a / 0x6134706D), 48000 Hz, stereo, fltp, 192 kb/s (default)
ffmpeg \
		-y \
		-i "${BUILD_PATH}/${name}_tn.png" \
		-c:v libx264 \
		-pix_fmt yuv420p \
		"${BUILD_PATH}/thumbnail.mp4"
ffmpeg \
		-y \
		-i "${BUILD_PATH}/thumbnail.mp4" \
		-f lavfi -i anullsrc=cl=stereo:r=48000 \
		-c:a mp3 \
		-c:v copy \
		-shortest \
		-map 0:v -map 1:a \
		"${BUILD_PATH}/thumbnail-a.mp4"
ffmpeg \
		-y \
		-f concat \
		-safe 0 \
		-i <( printf "file '%s'\nfile '%s'\n" "${PWD}/${BUILD_PATH}/thumbnail-a.mp4" "${PWD}/${BUILD_PATH}/${name}.mp4" ) \
		-c copy \
		"${BUILD_PATH}/tn-${name}.mp4"
#rm "${BUILD_PATH}/thumbnail.mp4"
