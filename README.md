# Animace FIT pro DOD

Animace vyjadřující motto: "FIT (je) cestou do všech odvětví" pro Den Otevřených Dveří FIT ČVUT v Praze. Animace je primárně určena pro zařzení HoloMotion, které zobrazuje video pomocí rotujících ramen s LED pásky. Proto jsou animace čtvercové a na černém pozadí, které pak vypadá jako průhledné, takže se animace jakoby vznáší v prostoru (hologram). Proto také animace vyžívají masivně 3D efekt.

**[Všechny vyrenderované animace jsou dostupné ke stažení](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/packages) nebo ke shlédnutí na této stránce (dále).**

Animace je vytvořena jako nekonečná smyčka, která postupně představuje jednotlivé "odvětví" ve formě ikon a přitom se stále vrací k mottu. Jedná se tedy o 2 smyčky v sobě,
```
MOTTO - IKONA1 - MOTTO - IKONA2 - MOTTO - IKONA3 - ...
```
takže může fungovat jak krátkodobě (několik sekund) pro sdělení motta, tak i dlouhodobě (cca jednotky minut), aby vzbudila zvědavost, jaká další animace bude následovat.

![Plná animace s mottem před každou ikonou](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/798287/artifacts/raw/build/tn-full.mp4)  
[Plná animace bez úvodního náhledu](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/artifacts/all/raw/build/full.mp4?job=build)


Ikon je celkem 11, ale není problém kteroukoliv vyměnit, ostranit nebo naopak přidat další. Celková délka animace je pak součtem délky motta a jednotlivé ikony vynásobená počtem ikon.

```
T_celkem = ( T_motto + T_ikony ) * počet_ikon
```

Při cca 10s na 1 ikonu i motto by byla délka animace 11 * 20s = 220s = cca 4min. Hlavní sdělení (motto) by přitom bylo každých cca 10s, takže by mělo velkou šanci, aby se dostalo i k náhodným kolemjdoucím.

Pro jiné použití je k dispozici také verze videa, kde na začátku je motto následované jednotlivými ikonami. Tedy motto se neopakuje před každou ikonou.

![Animace všech ikon s jediným mottem](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/798287/artifacts/raw/build/tn-icons.mp4)  
[Plná animace bez úvodního náhledu](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/artifacts/all/raw/build/icons.mp4?job=build)


## Motto [![pipeline status](https://gitlab.fit.cvut.cz/barinkl/dod-animation/badges/motto/pipeline.svg)](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/commits/motto)

Motto "FIT - cestou do všech odvětví" je ztvárněno prvoplánovitě jako cesta a křižovatka do mnoha směrů. Text i grafika má podobu dopravního značení. Jednotícím prvkem všech animací je podtržítko jako symbol programování. Proto každá animace začíná blikajícím kruzorem (začátek, očekávání).

Jednotlivá slova jsou tedy znázorněna graficky:
* **cestou** - ubíhající cesta tvořena podtržítky (jako přerušovaná čára na silnici)
* **do všech** - směr (do) je vyjádřen šipkami, kterých je více (všech)
* **odvětví** - z šipek se stávají větve, které rostou vzhůru
* **FIT** - z jednotlivých větví roste nápis FIT


![Animace motta](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/797333/artifacts/raw/build/tn-motto.mp4)  
[Animace motta bez úvodního náhledu](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/artifacts/motto/raw/build/motto.mp4?job=build)

1. [1s] blikající kurzor (podtržítko)
1. [1s] kurzor se stane 3D a protáhne do nekonečna
1. [1s] pruhy na cestě, přerušovaná čára (kamera jede po rovné cestě)
1. [1s] kamera se sklání kolmo dolů
1. [1s] objevuje se text "cestou"
1. [1s] z přerušované čáry se vytvoří rozcestí (přikázaný směr)
1. [1s] přidá se text "do všech odvětví"
1. [1s] rozcestí se (jako had) rozvětví do 3 paralelních čar
1. [1s] z čar vznikne text "FIT"
1. [1s] text "FIT" se zvětší a přitom se ukáže jeho 3D podoba (hloubka)
1. [1s] kamera proletí písmenem "I"


## Ikony

Jednotlivé odvětví jsou vyjádřeny ikonami. Animace každé ikony je taková, že se postupně ikona vykreslí 2D a pak se otočí kolem svislé osy stane se 3D ikonou s drobnou animací. Po pootočení 2D ikony se typicky začnou boční stěny vykreslovat do 3. rozměru (vytahovat), pokud není ikona již od začátku 3D. 

![Seznam ikon](src/icons.png)

Každá animace začíná jednotícím podtržítkem (viz výše). Podtřítko se pak stává základem, ze kterého vyrůstá ikona. Pokud je to možné, začátek animace a kurzor je umístěn vlevo dole.

Po vykreslení ikony se vypíše první (nejdůležitější) pojem, který opět začíná z podtržítka. Následně text odrotuje dolů (otáčí se) a objevují se další pojmy. První pojem je tedy vidět nejdéle. Pojmy cyklicky rotují až do vymizení.

1. [1s] blikající kurzor (podtržítko)
1. [2s] vytvoření 2D verze ikony
1. [2.5s] vytvoření 3D verze ikony
1. [2.5s] vypsání primárního text
1. [2.5s] animace ikony
1. [1s] animace ikony + přechod do černé (fade-out)

Jelikož použité ikony jsou obecnějšího charakteru, je možné si pod každou ikonou představit více pojmů a odvětví. Implementačně není problém pojmy upravit nebo změnit jejich pořadí.

* **01 - bankovnictví** (graf) [![pipeline status](https://gitlab.fit.cvut.cz/barinkl/dod-animation/badges/i01-bank/pipeline.svg)](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/commits/i01-bank)  
  měnící se sloupce a chvějící se šipka  
  ![Animace 01-bank](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/797349/artifacts/raw/build/tn-i01-bank.mp4)  
  [Animace 01-bank bez úvodního náhledu](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/artifacts/i01-bank/raw/build/i01-bank.mp4?job=build)
  * BANKOVNICTVÍ 
  * ANALÝZA DAT 
  * MANAGEMENT 
  * EKONOMIKA 
* **02 - automobilový průmysl** (auto) [![pipeline status](https://gitlab.fit.cvut.cz/barinkl/dod-animation/badges/i02-auto/pipeline.svg)](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/commits/i02-auto)  
  KITT z Kniht Ridera  
  ![Animace 02-auto](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/797909/artifacts/raw/build/tn-i02-auto.mp4)  
  [Animace 02-auto bez úvodního náhledu](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/artifacts/i02-auto/raw/build/i02-auto.mp4?job=build)
  * AUTOMOTIVE 
  * DOPRAVA 
  * ELEKTROMOBILITA 
  * AUTONOMNÍ ŘÍZENÍ 
* **03 - telekomunikace** (mobil) [![pipeline status](https://gitlab.fit.cvut.cz/barinkl/dod-animation/badges/i03-telco/pipeline.svg)](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/commits/i03-telco)  
  problikne se znak signálu a objeví se další zařízení  
  ![Animace 03-telco](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/797914/artifacts/raw/build/tn-i03-telco.mp4)  
  [Animace 03-telco bez úvodního náhledu](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/artifacts/i03-telco/raw/build/i03-telco.mp4?job=build)
  * TELEKOMUNIKACE 
  * MOBILNÍ APLIKACE 
  * INTERNET VĚCÍ
  * POČÍTAČOVÉ SÍTĚ 
* **04 - výrobní průmysl** (robot) [![pipeline status](https://gitlab.fit.cvut.cz/barinkl/dod-animation/badges/i04-robot/pipeline.svg)](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/commits/i04-robot)  
  pohyb ramene robota a přemístění objektu  
  ![Animace 04-robot](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/797917/artifacts/raw/build/tn-i04-robot.mp4)  
  [Animace 04-robot bez úvodního náhledu](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/artifacts/i04-robot/raw/build/i04-robot.mp4?job=build)
  * ROBOTIKA
  * PRŮMYSL
  * STROJÍRENSTVÍ
  * VÝROBA
* **05 - ekologie** (planeta) [![pipeline status](https://gitlab.fit.cvut.cz/barinkl/dod-animation/badges/i05-eco/pipeline.svg)](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/commits/i05-eco)  
  růst rostliny  
  ![Animace 05-eco](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/797922/artifacts/raw/build/tn-i05-eco.mp4)  
  [Animace 05-eco bez úvodního náhledu](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/artifacts/i05-eco/raw/build/i05-eco.mp4?job=build)
  * EKOLOGIE
  * UDRŽITELNOST
  * KLIMATOLOGIE
  * ŽIVOTNÍ PROSTŘEDÍ
* **06 - energetika** (stožár) [![pipeline status](https://gitlab.fit.cvut.cz/barinkl/dod-animation/badges/i06-energo/pipeline.svg)](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/commits/i06-energo)  
  pohyb jiskření po drátech k dalšímu stožáru  
  ![Animace 06-energo](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/797924/artifacts/raw/build/tn-i06-energo.mp4)  
  [Animace 06-energo bez úvodního náhledu](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/artifacts/i06-energo/raw/build/i06-energo.mp4?job=build)
  * ENERGETIKA 
  * ELEKTROINŽENÝRSVÍ
  * OBNOVITELNÉ ZDROJE
  * SMART GRIDS
* **07 - astronomie** (raketa) [![pipeline status](https://gitlab.fit.cvut.cz/barinkl/dod-animation/badges/i07-astro/pipeline.svg)](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/commits/i07-astro)  
  start rakety a nakreslení souhvězdí velkého vozu  
  ![Animace 07-astro](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/797925/artifacts/raw/build/tn-i07-astro.mp4)  
  [Animace 07-astro bez úvodního náhledu](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/artifacts/i07-astro/raw/build/i07-astro.mp4?job=build)
  * ASTRONOMIE 
  * KOSMONAUTIKA 
  * KARTOGRAFIE 
  * ASTROFYZIKA
* **08 - zdravotnictví** (srdce) [![pipeline status](https://gitlab.fit.cvut.cz/barinkl/dod-animation/badges/i08-bio/pipeline.svg)](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/commits/i08-bio)  
  pulzující srdce a vykreslování EKG  
  ![Animace 08-bio](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/797926/artifacts/raw/build/tn-i08-bio.mp4)  
  [Animace 08-bio bez úvodního náhledu](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/artifacts/i08-bio/raw/build/i08-bio.mp4?job=build)
  * ZDRAVOTNICVÍ 
  * BIOMEDICÍNA 
  * FARMACIE
  * BIOTECHNOLOGIE
* **09 - zemědělství** (traktor) [![pipeline status](https://gitlab.fit.cvut.cz/barinkl/dod-animation/badges/i09-agro/pipeline.svg)](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/commits/i09-agro)  
  pohled shora, jak traktor orá název FIT  
  ![Animace 09-agro](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/797927/artifacts/raw/build/tn-i09-agro.mp4)  
  [Animace 09-agro bez úvodního náhledu](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/artifacts/i09-agro/raw/build/i09-agro.mp4?job=build)
  * ZEMĚDĚLSTVÍ
  * AGROPRŮMYSL
  * POTRAVINÁŘSTVÍ
  * AGROEKOLOGIE
* **10 - multimédia** (video) [![pipeline status](https://gitlab.fit.cvut.cz/barinkl/dod-animation/badges/i10-media/pipeline.svg)](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/commits/i10-media)  
  ke trojůhelníku přibydou tečky a vše sežere pacman  
  ![Animace 10-media](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/797932/artifacts/raw/build/tn-i10-media.mp4)  
  [Animace 10-media bez úvodního náhledu](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/artifacts/i10-media/raw/build/i10-media.mp4?job=build)
  * MULTIMÉDIA 
  * HERNÍ PRŮMYSL 
  * VIZUÁLNÍ EFEKTY 
  * UX/UI
* **11 - stavebnictví** (dům) [![pipeline status](https://gitlab.fit.cvut.cz/barinkl/dod-animation/badges/i11-build/pipeline.svg)](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/commits/i11-build)  
  blikající okna a nárůst propojení dovnitř/ven  
  ![Animace 11-build](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/797938/artifacts/raw/build/tn-i11-build.mp4)  
  [Animace 11-build bez úvodního náhledu](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/artifacts/i11-build/raw/build/i11-build.mp4?job=build)
  * STAVEBNICTVÍ 
  * CHYTRÉ DOMY
  * CHYTRÁ MĚSTA 
  * ARCHITEKTURA 


## Technické detaily

Černá barva představuje nesvítící diody a tedy průhledný obraz. Čím světlejší jsou body, tím lépe je objekt vidět. Proto se používá jako hlavní barva bílá a jako doplňková barva (zvýrazňující) FIT barva (`#f0ab00`).

* Rozměr videa: 1080 x 1080
* FPS: 24
* Font: Technica (bold)


Schéma výsledných videí v [artefaktech](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/artifacts/main/browse/build?job=build)

* `full.mp4` - všechny animace dohromady ve formátu Motto-Ikona1-Motto-Ikona2-Motto-…  
  *vznikne skriptem `concat.sh`*
* `icons.mp4` - všechny animace dohromady ve formátu Motto-Ikona1-Ikona2-Ikona3-…  
  *vznikne skriptem `concat.sh`*
* `m-i??-*.mp4` - animace jedné ikony s mottem na začátku  
  *vznikne skriptem `add_motto.sh`*
* `i??-*.mp4` - samostatná animace jedné ikony  
  *vznikne programem Blender (4.0), resp. skriptem `render.sh`, jsou nahrané v adresáři*
* `tn-*.mp4` - animace s náhledovým snímkem na začátku videa (pro přehrávače, které zobrazují první snímek jako náhled videa)  
  *vznikne skriptem `thumbnail.sh`*

Názvy animací: `motto` `i01-bank` `i02-auto` `i03-telco` `i04-robot` `i05-eco` `i06-energo` `i07-astro` `i08-bio` `i09-agro` `i10-media` `i11-build`

Pro hudební podkres byly zvoleny 2 krátké skladby z [YouTube Audio Library](https://www.youtube.com/audiolibrary?feature=blog), které jsou volně k použití. Skladba pod ikony byla upravena (několikrát zopakovaný začátek), aby svou délkou odpovídala animaci. Rytmus animací je přizpůsoben hudebnímu doprovodu.

* Motto: [Mad Science (Sting) - MK2.mp3](src/Mad%20Science%20(Sting)%20-%20MK2.mp3)
* Ikony: [Leavin (Sting) - MK2.mp3](src/Leavin%20(Sting)%20-%20MK2.mp3)



## Render v rámci CI/CD

Všechny videa je možné vytvořit automaticky (headless). K tomuto účelu existuje několik skriptů, které volají buď `blender` pro render videí nebo `ffmpeg` pro jejich spojování.

* [`render.sh`](render.sh)  
  Do adresáře `build` vyrenderuje animace souborů zadaných jako argumenty. Render probíhá ve 2 fázích: nejdříve render jednotlivých snímků animace do adresáře `cache*` ve formátu PNG a následně spojení snímků z cache se závěrečným přechodem do černé a doplnění audia.  
  *Použití:* `./render.sh *.blend`
* [`add_motto.sh`](add_motto.sh)  
  Před každou animaci ikony v adresáři (`build/i[0-9]*.mp4`) přidá motto.  
  *Použití:* `./add_motto.sh`
* [`concat.sh`](concat.sh)  
  Sestaví 2 videa: `full` a `icons` (viz dále) z motta a jednotlivých animací ikon v adresáři `build`.  
  *Použití:* `./concat.sh`
* [`thumbnail.sh`](thumbnail.sh)  
  Vytvoří kopii videa s náhledovým obrázkem podle zadaného blend souboru a čísla snímku animace.  
  *Použití:* `./thumbnail.sh motto.blend 123


Celá práce je rozdělena do několika větví, aby při změně jedné části nebylo potřeba renderovat všechno a také pro dodržení limitu na délku běhu jedné úlohy (job) v rámci CI/CD. Tím je možné také paralelizace renderu, kdy jednotlvé úlohy mohou běžet paralelně. Výsledky renderu jsou dostupné v artefaktech dané větve (a posledního jobu).

V repozitáři `main` se stahují výsledky renderů ze všech větví a publikují v rámci
* ["balíčku" (`Deploy > Package registry`)](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/packages) a
* ["artefaktů" (`Build > Artifacts`)](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/jobs/artifacts/main/browse/build?job=build)
* ["dokumentace - README.md" (`Repository`)](https://gitlab.fit.cvut.cz/barinkl/dod-animation/-/blob/main/README.md)

GitLab umožňuje zobrazení výsledků CI/CD (artefaktů) v rámci (např.) README.md. Pro obrázky je možné sáhnout do poslední úspěšné úlohy a zobrazit je. Totéž však nefunguje pro videa. Nedojde totiž k vnitřnímu přesměrování z generického názvu úlohy (poslední úspěšný job) na konkrétní úlohu (podle ID).

Řešením tohoto problému je zjistit po každé změně číslo poslední úlohy a do souboru `README.md` uložit tuto novou adresu. Proto ještě existují skripty, které aktualizaci adres zajišťují:
* [`update-artifacts.sed`](update-url.sh)  
  Projde zadaný (markdown) soubor a každou fixní adresu jobu nahradí za novou adresu posledního úspěšného jobu pomocí následujícího skriptu  
  *Použití:* `./update-artifacts.sed README.md`
* [`update-url.sh`](update-url.sh)  
  Pro zadanou adresu jobu vypíše adresu posledního úspěšného jobu ve stejné větvi  
  *Použití:* `./update-url.sh prefix URL suffix`
