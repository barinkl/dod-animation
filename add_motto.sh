#!/usr/bin/env bash

: "${BUILD_PATH:=build}"

err() {
	printf "$0[error]: %s\n" "$@" >&2
	exit 1
}

[ -d "${BUILD_PATH}" ] || err "Directory '${BUILD_PATH}' not available, create the directory (using ./render.sh script) or set BUILD_PATH env. variable."
[ -f "${BUILD_PATH}/motto.mp4" ] || err "Motto animation '${BUILD_PATH/motto.mp4}' not foung, create animation (using ./render.sh script)."

cd "${BUILD_PATH}" || exit 1

for i in i[0-9]*.mp4; do
	ffmpeg \
		-y \
		-f concat \
		-safe 0 \
		-i <(printf "file '$PWD/motto.mp4'\nfile '$PWD/%s'\n" "$i") \
		-c copy \
		"m-$i"
done
