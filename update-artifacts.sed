#!/usr/bin/env -S sed -E -f

\,/-/jobs/[0-9]+/, {
	s,(.*\()([^)]*/-/jobs/[0-9]+/[^)]*)(\).*),./update-url.sh '\1' '\2' '\3',e
	s,\r,,g
}
