#!/usr/bin/env bash

: "${BLENDER_PATH:=/usr/local/blender/blender}"
: "${BUILD_PATH:=build}"

blender() {
  "${BLENDER_PATH}" "$@"
}

for source; do
  blender -b "${source}" -S '3D' -a
  blender -b "${source}" -S 'Render' -o "${BUILD_PATH}/${source%.blend}.mp4" -a
done
