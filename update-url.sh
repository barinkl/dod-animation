#!/usr/bin/env bash

# https://gitlab.fit.cvut.cz/barinkl/blender-render-test/-/jobs/123/artifacts/raw/build/tn-motto.mp4
# https://gitlab.fit.cvut.cz/barinkl/blender-render-test/-/jobs/artifacts/motto/raw/build/tn-motto.mp4?job=build


new_url() {
	old="$1"
	generic=$(
		sed -E '
			s,/-/jobs/[0-9]+/,/-/jobs/,
			s,^(.*/-/jobs/artifacts/)(.*/)(tn-)?([^/]*)(\.[^/]*)$,\1\4/\2\3\4\5,
			s,/artifacts/(full|icons)/,/artifacts/all/,
			s,$,?job=build,
		' <<< "${old}"
	)
	curl -I "${generic}" 2>/dev/null | sed -n 's/^location: *//Ip' | tr -d '\r'
}

#printf '===%s===\n' "$@" >&2

printf '%s%s%s' "$1" "$( new_url "$2" )" "$3"
